import { createStore, applyMiddleware, compose } from "redux"
import { createLogger } from "redux-logger"
import createSagaMiddleware, { END } from "redux-saga"

import { rootReducer } from "../reducers"
import { sagas } from "../sagas"

export default function configureStore(initialState) {
    const sagaMiddleware = createSagaMiddleware();

    const store = createStore(
        rootReducer,
        initialState,
        compose(
            applyMiddleware(
                sagaMiddleware,
                createLogger()
            )
        )
    );

    if (module.hot) {
        module.hot.accept('../reducers', () => {
            const nextRootReducer = require('../reducers').default;
            store.replaceReducer(nextRootReducer);
        });
    }

    sagaMiddleware.run(sagas);
    store.close = () => store.dispatch(END);

    return store;
}