import React from "react"
import styles from "./app.scss"

import TopBar from "./components/top-bar/top-bar"
import Navigation from "./components/navigation/navigation"
import Routes from "./routes"
import ConnectedRouter from "./components/connected-router/connected-router"

const App = ({ history }) => (
    <ConnectedRouter history={history}>
        <div className={styles.root}>
                <header>
                    <TopBar />
                </header>
                <nav>
                    <Navigation />
                </nav>
                <main>
                    <Routes />
                </main>
        </div>
    </ConnectedRouter>
);

export default App;