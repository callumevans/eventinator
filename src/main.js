import "@babel/polyfill"

import React from "react"
import ReactDOM from "react-dom"
import { Provider } from "react-redux"
import { createBrowserHistory } from "history"
import initStore from "./store"

import "normalize.css"

import ("./fontawesome");
import ("./fa-light.js");

import App from "./app"

const store = initStore();
const history = createBrowserHistory();

ReactDOM.render(
    <Provider store={store}>
        <App history={history} />
    </Provider>,
document.getElementById('app'));

if (module.hot) {
    module.hot.accept();
}