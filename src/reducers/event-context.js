import { Actions } from "../actions/constants"

const initialState = {
};

const eventContextReducer = (state = initialState, action) => {
    switch (action.type) {
        case Actions.EventContextSet:
            return {
                ...action.eventContext
            };
        default:
            return state;
    }
};

export default eventContextReducer;