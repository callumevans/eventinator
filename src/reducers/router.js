import { Actions } from "../actions/constants"
import Route from "route-parser"

const eventRoute = new Route('/events/:eventId');

const initialState = {
};

const routerReducer = (state = initialState, action) => {
    switch (action.type) {
        case Actions.RouteChanged:
            return {
                ...state,
                eventId: eventRoute.match(action.route.pathname).eventId || null
            };
        default:
            return state;
    }
};

export default routerReducer;