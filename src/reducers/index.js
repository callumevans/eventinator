import { combineReducers } from "redux"

import routerReducer from "./router"
import eventContextReducer from "./event-context"

export const rootReducer = combineReducers({
    router: routerReducer,
    eventContext: eventContextReducer
});