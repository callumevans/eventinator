import React from "react"
import { Route, Switch } from "react-router-dom"

import PageLayout from "./components/page-layout/page-layout"
import EventsPage from "./pages/events-page/events-page"
import ManageEventPage from "./pages/manage-event-page/manage-event-page";

const routes = [
    {
        path: '/',
        exact: true,
        component: () => <span>Hello home!</span>
    },
    {
        path: '/events',
        exact: true,
        component: () => <EventsPage />
    },
    {
        path: '/events/:eventId',
        component: () => <ManageEventPage />
    }
];

const Routes = () => (
    <Switch>
        <PageLayout>
            { routes.map((route, index) =>
                <Route
                    key={index}
                    path={route.path}
                    exact={route.exact}
                    render={route.component} />
            )}
        </PageLayout>
    </Switch>
);

export default Routes;