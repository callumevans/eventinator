import { Actions } from "./constants"

export const routeChange = () => ({
    type: Actions.RouteChanged
});