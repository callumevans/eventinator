import React from "react"
import { NavLink } from "react-router-dom"

import { connect } from "react-redux"

import EventsFetcher from "../../services/events-fetcher/events-fetcher"

class EventsPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            events: []
        }
    }

    componentDidMount() {
        EventsFetcher.getEvents()
            .then((result) => {
                this.setState({
                    events: result
                })
            });
    }

    render() {
        return (
            <div>
                { this.state.events.map((event, i) =>
                    <div key={i}>
                        <NavLink to={`/events/${event.id}`}>{event.name}</NavLink>
                    </div>)
                }
            </div>
        )
    }
}

const mapStateToProps = state => ({
    reduxState: state
});

export default connect(mapStateToProps)(EventsPage);