import { call, put, takeEvery, select } from "redux-saga/effects"
import { Actions } from "../actions/constants"
import EventsFetcher from "../services/events-fetcher/events-fetcher"

let currentEventId = null;

function* getEvent() {
    const nextEventId = yield select(state => state.router.eventId);

    if (currentEventId !== nextEventId) {
        const event = !!nextEventId
            ? yield call(() =>  EventsFetcher.getEvent(nextEventId))
            : yield null;

        yield put({ type: Actions.EventContextSet, eventContext: event });
        currentEventId = nextEventId;
    }
}

export function* sagas() {
    yield(takeEvery(Actions.RouteChanged, getEvent))
}