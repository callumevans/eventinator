import React from "react"
import { connect } from "react-redux"
import styles from "./navigation.scss"

import MainNav from "./main-nav/main-nav"
import EventSubNav from "./event-sub-nav/event-sub-nav"

const Navigation = ({ isEventContextSet }) => {
    return (
        <div className={styles.container}>
            <MainNav minimised={isEventContextSet}/>
            { isEventContextSet &&
                <aside className={styles.subNavContainer}>
                    <EventSubNav/>
                </aside>
            }
        </div>
    )
};

const mapStateToProps = state => ({
    isEventContextSet: state.eventContext && state.eventContext.id
});

export default connect(mapStateToProps, null, null, { pure: false })(Navigation);