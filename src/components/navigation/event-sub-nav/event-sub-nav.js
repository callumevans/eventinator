import React from "react"
import styles from "./event-sub-nav.scss"

const SubNavItem = ({ text }) => (
    <li className={styles.item}>{text}</li>
);

const SubNavSection = ({ children, heading }) => (
    <section className={styles.subNavGroup}>
        <div className={styles.sectionHeading}>{heading}</div>
            <ul className={styles.itemsWrapper}>
                {children}
            </ul>
    </section>
);

const EventSubNav = () => (
    <div className={styles.subNav}>
        <SubNavSection heading="Setup" icon="fal fa-book-open">
            <SubNavItem text="Name and Description" />
        </SubNavSection>

        <SubNavSection heading="Details" icon="fal fa-book-open">
            <SubNavItem text="Name and Summary" />
        </SubNavSection>

        <SubNavSection heading="Payments" icon="fal fa-book-open">
            <SubNavItem text="Name and Summary" />
        </SubNavSection>
    </div>
);

export default EventSubNav;