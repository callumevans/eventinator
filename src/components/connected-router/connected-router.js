import React from "react"

import { connect } from "react-redux"
import { Router } from "react-router-dom"
import { Actions } from "../../actions/constants"

class ConnectedRouter extends React.Component {
    componentDidMount() {
        this.props.history.listen((location) => {
            this.props.onRouteChanged(location);
        });

        this.props.onRouteChanged(this.props.history.location)
    }

    componentWillUnmount() {
        this.props.history.listen = null;
    }

    render() {
        return (
            <Router history={this.props.history}>
                <div>
                    {this.props.children}
                </div>
            </Router>
        )
    }
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => ({
    onRouteChanged: (route) => dispatch({ type: Actions.RouteChanged, route: route })
});

export default connect(mapStateToProps, mapDispatchToProps, null, { pure: false })(ConnectedRouter);