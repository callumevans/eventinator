import React from "react"
import styles from "./page-layout.scss"
import { connect } from "react-redux"

const PageLayout = ({ pageTitle, children }) => (
    <div>
        { pageTitle && <h1 className={styles.pageTitle}>{pageTitle}</h1> }
        <div className={styles.pageContent}>
            { children }
        </div>
    </div>
);

const mapStateToProps = state => ({
    pageTitle: state.eventContext.name
});

export default connect(mapStateToProps)(PageLayout);