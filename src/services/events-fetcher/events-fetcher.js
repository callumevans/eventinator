const EventsFetcher = {
    getEvents() {
        return new Promise((resolve) =>
            resolve([
                { id: 'friendly-event', name: 'Friendly Event' },
                { id: 'other-event', name: 'Other Event' },
                { id: 'that-event', name: 'That Event' },
            ])
        );
    },

    getEvent(eventId) {
        return new Promise((resolve => {
            this.getEvents().then((events) => {
                resolve(events.find(event => event.id === eventId))
            })
        }))
    }
};

export default EventsFetcher;